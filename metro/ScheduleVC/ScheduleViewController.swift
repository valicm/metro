//
//  ScheduleViewController.swift
//  metro
//
//  Created by Valentyn Mialin on 10/22/18.
//  Copyright © 2018 Valentyn Mialin. All rights reserved.
//

import UIKit

class ScheduleViewController: UIViewController, TheDelegate {

    @IBOutlet weak var tableView: UITableView!
    private let cellIdentifier = "scheduleCell"
    
    var schedules = [Schedule]()
    var groupSortHours = GroupSortHours()
    
    var maxCountMinute = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        self.groupSortHours = sortDate(schedules)
        tableView.reloadData()
    }

    private func sortDate(_ schedules: [Schedule]) -> GroupSortHours {
        var groupSort = GroupSortHours()
    
        schedules.forEach { (schedule) in
            let hour = Int(schedule.hour)
            let minute = Int(schedule.minute)
            if let index = groupSort.firstIndex(where: { $0.hour == hour }) {
                groupSort[index].minute.append(minute)
            }
            else{
                groupSort.append(GroupSortHour(hour: hour, minute: [minute]))
            }
        }
        groupSort = groupSort.sorted(by: {$0.hour < $1.hour})
        for (index, group) in groupSort.enumerated() {
            groupSort[index].minute = group.minute.sorted(by: <)
            let minuteCount = groupSort[index].minute.count
            if minuteCount > maxCountMinute {
                maxCountMinute = minuteCount
            }
        }

        return groupSort
    }
    
    func didScroll(to position: CGFloat) {
        for cell in tableView.visibleCells as! [ScheduleTableViewCell] {
            (cell.collectionView as UIScrollView).contentOffset.x = position
        }
    }
    
}

extension ScheduleViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupSortHours.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ScheduleTableViewCell
        cell.hourLabel?.text = String(groupSortHours[indexPath.row].hour)
        cell.hour = groupSortHours[indexPath.row].hour
        cell.minute = groupSortHours[indexPath.row].minute
        cell.maxCount = maxCountMinute
        cell.scrollDelegate = self

        return cell
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

//MARK: - UITableViewDelegate
extension ScheduleViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.performSegue(withIdentifier: "scheduleSegue", sender: indexPath)
//    }
}
