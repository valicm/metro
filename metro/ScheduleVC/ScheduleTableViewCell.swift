//
//  ScheduleTableViewCell.swift
//  metro
//
//  Created by Valentyn Mialin on 10/22/18.
//  Copyright © 2018 Valentyn Mialin. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!

    var hour = 0
    var minute = [Int](){
        didSet{
             collectionView.reloadData()
        }
    }
    var maxCount = 0
    weak var scrollDelegate: TheDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        (collectionView as UIScrollView).delegate = self
        collectionView.dataSource = self
    }
    
    
}

extension ScheduleTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return maxCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "minuteCell", for: indexPath) as! MinuteCollectionViewCell
        
        if minute.indices.contains(indexPath.row) {
            cell.isHidden = false
            let hour = self.hour

            let (timeHour, timeMinute) = DateComponent.timeZoneKiev()
            print("\(timeHour) \(timeMinute)")
            print(hour)
            
            let hourTemp = 12
            let minuteTemp = 1
            if hour >= hourTemp {
                if hour == hourTemp {
                    if minute[indexPath.row] > minuteTemp {
                        if indexPath.row > 0 {
                            if minute[indexPath.row - 1] > minuteTemp {
                                cell.minuteLabel?.textColor = .green
                            }
                            else{
                                cell.minuteLabel?.textColor = .black
                            }
                        }
                        else{
                            cell.minuteLabel?.textColor = .black
                        }
                    }
                    else{
                        cell.minuteLabel?.textColor = .red
                    }
                }
                else{
                    cell.minuteLabel?.textColor = .green
                }
            }
            else{
                cell.minuteLabel?.textColor = .red
            }
            cell.minuteLabel?.text = String(minute[indexPath.row])
        }
        else{
            cell.minuteLabel?.text = ""
            cell.isHidden = true
        }
        return cell
    }
}

extension ScheduleTableViewCell: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollDelegate?.didScroll(to: scrollView.contentOffset.x)
    }
}

protocol TheDelegate: class {
    func didScroll(to position: CGFloat)
}


