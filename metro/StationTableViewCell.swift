//
//  StationTableViewCell.swift
//  metro
//
//  Created by Valentyn Mialin on 10/22/18.
//  Copyright © 2018 Valentyn Mialin. All rights reserved.
//

import UIKit

class StationTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var nameStationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(station: Station) {
        switch Locale.current.languageCode {
        case "uk":
            self.nameStationLabel?.text = station.titleUA
        case "ru":
            self.nameStationLabel?.text = station.titleRU
        default:
            self.nameStationLabel?.text = station.titleEN
        }
    }
}


class StationTopAndBottomTableViewCell: StationTableViewCell {
    
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var bottomLineView: UIView!
    @IBOutlet weak var circleStationLabel: UILabel!
  
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//        
//        // Configure the view for the selected state
//    }
    
    func settingCell(){
        
    }
}
