//
//  StationDataSource.swift
//  metro
//
//  Created by Valentyn Mialin on 10/22/18.
//  Copyright © 2018 Valentyn Mialin. All rights reserved.
//

import UIKit
import CoreData

final class StationDataSource: NSObject, UITableViewDataSource {
     var labelCircleView: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    
    lazy var fetchedhResultController = NSFetchedResultsController<NSFetchRequestResult>()
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = fetchedhResultController.sections?.first?.numberOfObjects {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let station = fetchedhResultController.object(at: indexPath) as?  Station {
            
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "stationTopTableViewCell") as! StationTopAndBottomTableViewCell
                cell.update(station: station)
                if station.id == 1 {
                    cell.circleStationLabel.text = "A"
                }
                else{
                    cell.circleStationLabel.text = "B"
                }
                cell.topLineView?.isHidden = true
                cell.bottomLineView?.isHidden = false
                return cell
                
            case self.tableView(tableView, numberOfRowsInSection: 0) - 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "stationTopTableViewCell") as! StationTopAndBottomTableViewCell
                cell.update(station: station)
                if station.id == 1 {
                    cell.circleStationLabel.text = "A"
                }
                else{
                    cell.circleStationLabel.text = "B"
                }
                cell.topLineView?.isHidden = false
                cell.bottomLineView?.isHidden = true
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "stationTableViewCell") as! StationTableViewCell
                cell.update(station: station)
                return cell
            }
        }
        return StationTableViewCell()
    }
}


extension StationDataSource {
    subscript(indexPath: IndexPath) ->  Station {
        return fetchedhResultController.object(at: indexPath) as!  Station
    }
}
