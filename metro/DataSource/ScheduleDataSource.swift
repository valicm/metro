//
//  ScheduleDataSource.swift
//  metro
//
//  Created by Valentyn Mialin on 10/22/18.
//  Copyright © 2018 Valentyn Mialin. All rights reserved.
//

//import UIKit
//
//class ScheduleDataSource: NSObject, UITableViewDataSource {
//
//    private let cellIdentifier = "scheduleCell"
//
//    var groupSortHours = GroupSortHours()
//
//
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return groupSortHours.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ScheduleTableViewCell
//        cell.hourLabel?.text = String(groupSortHours[indexPath.row].hour)
//        cell.minute = groupSortHours[indexPath.row].minute
//        cell.scrollDelegate = self
//        return cell
//    }
//
//
//
//
//
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return groupSortHours.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return groupSortHours[section].minute.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ScheduleCellCollectionViewCell
//        cell.minuteLabel?.text = String(groupSortHours[indexPath.section].minute[indexPath.row])
//        return cell
//    }
    

    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//         let header = UICollectionReusableView()
//        header.tintColor = UIColor.red
//        return header
//    }
//}

typealias GroupSortHours = [GroupSortHour]

struct GroupSortHour {
    var hour: Int
    var minute: [Int]
}
