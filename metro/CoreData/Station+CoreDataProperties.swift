//
//  Station+CoreDataProperties.swift
//  metro
//
//  Created by Valentyn Mialin on 10/21/18.
//  Copyright © 2018 Valentyn Mialin. All rights reserved.
//
//

import Foundation
import CoreData


extension Station {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Station> {
        return NSFetchRequest<Station>(entityName: "Station")
    }

    @NSManaged public var id: Int16
    @NSManaged public var titleEN: String?
    @NSManaged public var titleRU: String?
    @NSManaged public var titleUA: String?
    @NSManaged public var schedule: NSSet?

}

// MARK: Generated accessors for schedule
extension Station {

    @objc(addScheduleObject:)
    @NSManaged public func addToSchedule(_ value: Schedule)

    @objc(removeScheduleObject:)
    @NSManaged public func removeFromSchedule(_ value: Schedule)

    @objc(addSchedule:)
    @NSManaged public func addToSchedule(_ values: NSSet)

    @objc(removeSchedule:)
    @NSManaged public func removeFromSchedule(_ values: NSSet)

}
