//
//  Schedule+CoreDataProperties.swift
//  metro
//
//  Created by Valentyn Mialin on 10/21/18.
//  Copyright © 2018 Valentyn Mialin. All rights reserved.
//
//

import Foundation
import CoreData


extension Schedule {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Schedule> {
        return NSFetchRequest<Schedule>(entityName: "Schedule")
    }

    @NSManaged public var hour: Int16
    @NSManaged public var idDirect: Bool
    @NSManaged public var isWeekend: Bool
    @NSManaged public var minute: Int16
    @NSManaged public var station: Station?

}
