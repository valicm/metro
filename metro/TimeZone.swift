//
//  TimeZone.swift
//  metro
//
//  Created by Valentyn Mialin on 10/27/18.
//  Copyright © 2018 Valentyn Mialin. All rights reserved.
//

import Foundation

struct DateComponent {
    static func timeZoneKiev() -> (hour:Int, minute:Int) {
    
        let timezone = TimeZone(identifier: "Europe/Kiev")!
        
        let formatterHour = DateFormatter()
        formatterHour.timeZone = timezone
        formatterHour.dateFormat = "HH"
        
        let formatterMinute = DateFormatter()
        formatterMinute.timeZone = timezone
        formatterMinute.dateFormat = "mm"
        
        let date = Date()
        let hour = Int(formatterHour.string(from: date))!
        let minute = Int(formatterMinute.string(from: date))!
      
        return (hour, minute)
    }
}
