//
//  StationController.swift
//  metro
//
//  Created by Valentyn Mialin on 10/21/18.
//  Copyright © 2018 Valentyn Mialin. All rights reserved.
//

import UIKit
import CoreData

class StationController: UIViewController {
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    fileprivate var dataSource = StationDataSource()
    
    private let cellIdentifier = "stationTableViewCell"
    
    var station = Station()
    var isDirect = true
    
    lazy var fetchedhResultController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Station.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataStack.sharedInstance.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupTableView()
        updateTableContent()

    }
    
    //MARK: - Setting View Controller
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200.0
        tableView.separatorStyle = .none
    }
    
    //MARK: - Load Data
    private func updateTableContent() {
        do {
            self.fetchedhResultController.fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: isDirect)]
            self.dataSource.fetchedhResultController = self.fetchedhResultController
            try self.fetchedhResultController.performFetch()
            print("COUNT FETCHED FIRST: \(self.fetchedhResultController.sections?[0].numberOfObjects ?? 0)")
        } catch let error  {
            print("ERROR: \(error)")
        }
         tableView.reloadData()
    }
    @IBAction func direct(_ sender: Any) {
        isDirect = !isDirect
        updateTableContent()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "scheduleSegue",
            let indexPath = sender as? IndexPath {
            let selectedStation = dataSource[indexPath]
            
            guard let schedules = selectedStation.schedule?.allObjects as? [Schedule] else { return }
            let scheduleViewController = segue.destination as! ScheduleViewController
            scheduleViewController.schedules = schedules
        }
    }
}

//MARK: - UITableViewDelegate
extension StationController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! StationTableViewCell
//        let station = fetchedhResultController.object(at: indexPath) as!  Station
//         //   cell.circleView?.backgroundColor = .red
////            switch indexPath.row {
////            case 0:
////                cell.topLineView?.isHidden = true
////                cell.circleViewWidth.constant = 20
////                cell.circleView?.layer.cornerRadius = 10
////                cell.circleView?.backgroundColor = .green
////                cell.bottomLineView?.isHidden = false
////                
////                cell.circleView?.setNeedsDisplay()
////                
////                if station.id == 1 {
////                    cell.labelCircleView.text = "A"
////                }
////                else{
////                    cell.labelCircleView.text = "B"
////                }
////                cell.labelCircleView?.isHidden = false
////                
////            case self.fetchedhResultController.sections?[0].numberOfObjects ?? 1 - 1:
////                cell.topLineView?.isHidden = false
////                cell.circleViewWidth.constant = 20
////                cell.circleView?.layer.cornerRadius = 10
////                cell.circleView?.backgroundColor = .red
////                cell.bottomLineView?.isHidden = true
////                
////                if station.id == 1 {
////                    cell.labelCircleView.text = "A"
////                }
////                else{
////                    cell.labelCircleView.text = "B"
////                }
////                cell.labelCircleView?.isHidden = false
////                
////            default:
////                cell.topLineView?.isHidden = false
////                cell.circleViewWidth.constant = 8
////                cell.circleView?.layer.cornerRadius = 4
////                cell.circleView?.backgroundColor = .black
////                cell.bottomLineView?.isHidden = false
////                
////                cell.labelCircleView?.isHidden = true
////            }
//            
//          //  cell.update(station: station)
//      //  }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "scheduleSegue", sender: indexPath)
    }
}


//MARK: - NSFetchedResultsControllerDelegate
extension StationController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            self.tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            self.tableView.deleteRows(at: [indexPath!], with: .automatic)
        default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        UIView.performWithoutAnimation {
            self.tableView.endUpdates()
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        UIView.performWithoutAnimation {
            tableView.beginUpdates()
        }
    }
}
